
NOTICE:
- To upgrade the theme in your store, please follow the procedure which is described in the User Guide.
- In case of any questions about Ultimo theme: support requests can be send through our new support system:
https://support.infortis-themes.com

-----------------------------------


Version 2.3.1 - November 30, 2016

New:
- mega menu: option to show/hide bullets for subcategories on lower levels
- mobile menu: option to show/hide top-level categories which have no subcategories but have category blocks

Changed:
- refreshed CSS styles of the menu dropdowns (e.g. normalized spaces between items)
- mobile menu: option to hide category blocks when viewport is narrower than 320px now applies when viewport is narrower than 480px

Fixed:
- mini cart dropdown doesn't collapse automatically when user clicks inside the dropdown (e.g. to update the product quantity)
- if maximum page width is set to "1920 px" or "Full width", category view displays correct number of columns when viewport is very wide (1920 px or larger)
- if maximum page width is set to "Full width", page has unlimited width, instead of 1920 px
- removed flickering of the mini cart when loading page on mobile


Version 2.3.0 - November 2, 2016

New:
- add custom static block inside the top header in mobile view (custom static block can be added through Magento widgets to the container named "Page Mobile Header, Top"). Content of the block can be aligned left, right or centered.
- restored two built-in static blocks "block_header_top_left" and "block_header_top_right" (they were removed in earlier version). These blocks are automatically displayed in the top header.

Changed:
- design of toolbar and pager buttons in category view. Colors for these elements can be applied through admin panel.
- display of products list in the "list" mode in category view. It's now similar to theme version for Magento 1: "Add to cart" button and price is displayed in a column at the right side.
- display of "select all" button in the heading of the related products block on product page
- position of "Sign Up" link on checkout page
- removed "add to" links from bundle product configuration summary block on product page

Fixed:
- blocks in the footer and blocks in the sidebars are collapsed in mobile view 
- correct product images are displayed (instead of Magento placeholder image) in Featured Products slider when enabling "Keep Image Aspect Ratio" option under Configuration > Theme Settings > Product Slider
- dropdowns of currency switcher and store view switcher work correctly on iOS
- when a static block is added through Magento widgets to one of the top header containers (e.g. "Page Header, Top, Left" container), it is displayed on correct position. In previous version, each block added through widgets was displayed in a separate line which could split the top header into a few lines.
- top menu dropdowns are not overlapped by menu links
- store view switcher (language switcher) switches the store view permanently, not only on the current page. Also, page URLs are correctly translated when switching the language.
- in IE10 page content doesn't overlap the footer when page content is longer than the height of the viewport
- product page:
	- if product collateral data blocks on product page are displayed as vertically stacked blocks (i.e. tabs are disabled), the blocks are now displayed correctly and don't overlap each other
	- product image magnifier doesn't overlap dropdowns of the top menu
	- product ratings (stars) don't overlap product image magnifier
	- number of existing product reviews in the product collateral data block is now displayed inside brackets, e.g. "Reviews (3)"
	- when product listings are displayed inside primary column, the font size of product price inside product listings is not oversized 


Version 2.2.0 - September 14, 2016

New:
- files with theme settings to import from all demos
- files with sample content of home pages to import from all demos
- alternative product image can be selected based on the sort order of images (see "Select Alternative Image By" under Stores > Configuration > Theme Settings > Category View)
- added built-in static block "block_product_secondary_bottom"
- added an option to assign product page elements below the new static block "block_product_secondary_bottom"
- ability to set colors for "add to" icons on product page

Changed:
- removed side padding of filter options in layered navigation block to match padding of other sidebar blocks
- slightly improved page import

Fixed:
- category view grid:
	- colors of icons of "Add to wishlist" and "Add to compare" links on product page are correctly applied
	- "Add to Cart" buttons in category view can be hidden
	- "Add to Cart" buttons in category view can be centered correctly
	- all elements in category view are positioned correctly
	- all elements are aligned correctly to the center if grid in category view is set up as "Align Center"
	- alternative product image in category view and in featured products slider is now a clickable link
- other:
	- layered navigation on 1-column pages is displayed on correct position above products
	- colors settings are correctly applied to dropdown boxes in the header
	- top menu is aligned correctly inside sticky header when top menu is assigned to one of the columns inside primary header
	- logo inside sticky header can be disabled
	- product image inside mini cart is not clipped
	- option "Content Area Shadow" (under Stores > Configuration > Theme Design > Effects) correctly adds shadow to all page sections (e.g. primary footer) which have non-transparent "Inner Background Color"
	- sidebar menu has correct bottom margin
	- brand name is displayed correctly in the tooltip when mouse hover over brand logo (in brand slider and on product page). In previous version, tooltip displayed the following text: "Click to see more products from %s".
	- page import: if option "Overwrite Existing Pages" is selected, message after import ("Items with the following identifiers were overwritten: ...") displays correct list of overwritten pages.


Version 2.1.0 (August 13, 2016)

Fixed:
- mini cart icon in the sticky header is positioned correctly if the menu height was changed through admin settings
- custom links and the custom block in the top menu can be aligned to the right side
- home link icon is displayed with the same color as menu items
- logo in the sticky header is displayed correctly
- search box icon is positioned correctly
- heading of featured product slider has correct font size
- Slideshow:
	- correct slides are displayed if different slides (static blocks) are enabled in multiple store views
	- slideshow module doesn't cause error if no slides were set in the "Slides" field under Stores > Configuration > Slideshow > Main Slideshow on Home Page. In previous versions that could result in an exception: Invalid arguments passed in app\code\Infortis\UltraSlideshow\Block\Slideshow.php
	- image banners inside slides are correctly aligned vertically (no additional space below image)


Version 2.0.2 (July 30, 2016)

New:
- compatibility with Magento 2.1.0
- file with settings to import from Demo 2 (files from other demos will be added in next update)
- file with sample home page content to import from Demo 1 and Demo 2

Changes:
- minor CSS improvements
- changes of default values of breakpoints in sliders

Fixed:
- all CSS assets are loaded correctly via secure URLs
